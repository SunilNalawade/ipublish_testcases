use strict;
use warnings;
use Time::HiRes qw(sleep);
use Test::WWW::Selenium;
use Test::More "no_plan";
use Test::Exception;

my $sel = Test::WWW::Selenium->new( host => "localhost", 
                                    port => 4444, 
                                    browser => "*chrome", 
                                    browser_url => "http://beta.ipublish.india.com/" );

$sel->open_ok("/");
$sel->title_is("Free Websites by India.com | Get Free Domain Name & Host Your Website on India.com");
$sel->click_ok("css=a.sliderbtn.btnbgslider1 > span.btntxt");
$sel->click_ok("css=a.btntw > img");
my @wname = $sel->get_all_window_names();
print "@wname \n";
my $oneName = $wname[1];
print($oneName . "\n");
$sel->wait_for_pop_up_ok($oneName, "30000");
$sel->select_window_ok($oneName);
$sel->type_ok("id=username_or_email", "smithairoli\@gmail.com");
$sel->type_ok("id=password", "sohumn79");
$sel->click_ok("id=allow");
$sel->wait_for_page_to_load_ok("60000");
$sel->title_is("Twitter / Authorize an application");
$sel->click_ok("id=allow");
$sel->select_window_ok("null");
sleep 10;
my @windowtitles = $sel->get_all_window_titles();
print "@windowtitles  \n";
my $windowtitle = $windowtitles[0];
print($windowtitle . "\n");
if ($windowtitle eq "Sign Up for Free Website by India.com")
	{
				my $fname = $sel->get_value("id=fname");
				print($fname . "\n");
				my $lname = $sel->get_value("id=lname");
				print($lname . "\n");
				$sel->type_ok("id=email_id", "sunilnalawa\@de33\@gmail.com");
				$sel->type_ok("id=user_page_title", "12");
				$sel->click_ok("css=input.btnreqfrmSubmit");
				$sel->text_is("id=email_id_error", "Please Enter valid Email address");
				$sel->text_is("id=user_page_title_error", "Invalid Domain name.");
				#my @windowname = $sel->get_all_window_names();
				#print "@windowname \n";
	}
else
	{
				print("----------- Get user data from iPublish profile page ----------- ". "\n");
				my $name = $sel->get_text("css=h2");
				print("Name: ". $name . "\n");
				my $location = $sel->get_text("css=p.location");
				print("Location: ". $location . "\n");
				print("----------- Get user data from twitter login----------- ". "\n");
				$sel->open_ok("http://www.twitter.com/");
				sleep 10;
				#$sel->click_ok("css=span.caret");
				$sel->click_ok("css=small.metadata");
				sleep 10;
				my $twtname = $sel->get_value("id=user_name");
				print("Name: ". $twtname . "\n");
				my $twtlocation = $sel->get_value("id=user_location");
				print("Location: ". $twtlocation . "\n");
				print("----------- compare user data from iPublish with twitter profile details ----------- ". "\n");
				my $RESULT = $sel->get_eval($sel->get_eval("var result = '" . $name . "'=='" . $twtname . "';result;")&&("var result = '" . $location . "'=='" . $twtlocation . "';result;"));
				print($RESULT . "\n");

				$sel->open_ok("/");
				sleep 10;
				print("----------- Verify Add/Remove interest functionality ----------- ". "\n");
				$sel->click_ok("link=Add interest");
				$sel->wait_for_page_to_load_ok("60000");
				$sel->title_is("Manage Topics on India.com");
				print("----------- Enter topic to add ----------- ". "\n");
				$sel->type_ok("id=search_tag", "sachin tendulkar");
				$sel->click_ok("id=searchTopicsBtn");
				sleep(3);
				print("----------- Verify topic is populated ----------- ". "\n");
				$sel->text_is("css=p.prof-ttl", "Sachin Tendulkar");
				$sel->click_ok("css=p.prof-ttl");
				sleep(3);
				print("----------- Verify topic is added ----------- ". "\n");
				$sel->text_is("css=p.prof-ttl", "Sachin Tendulkar");
				$sel->click_ok("css=p.prof-ttl");
				sleep(3);
				print("----------- Verify topic is removed ----------- ". "\n");
				$sel->text_is("css=p.prof-ttl", "Sachin Tendulkar");
				$sel->click_ok("css=span.nvtxt");
				$sel->wait_for_page_to_load_ok("60000");
				$sel->title_is("smith1 on India.com | Get Latest News & Blogs by Smith Airoli at smith1.beta.ipublish.india.com");

				print("----------- Verify blog upload functionality----------- ". "\n");
				$sel->click_ok("link=Write a blog");
				$sel->wait_for_page_to_load_ok("60000");
				$sel->title_is("Free Websites by India.com | Get Free Domain Name & Host Your Website on India.com");
				print("----------- code to upload content using ckeditor----------- ". "\n");
				$sel->type_ok("id=post_title", "selenium ide steps for ckeditor");
				sleep 10;
				$sel->focus_ok("class=cke_editable cke_editable_themed cke_contents_ltr cke_show_borders");
				$sel->type_ok("class=cke_editable cke_editable_themed cke_contents_ltr cke_show_borders", "");
				$sel->type_keys_ok("class=cke_editable cke_editable_themed cke_contents_ltr cke_show_borders", "Hi IWPL");
				$sel->run_script_ok("CKEDITOR.instances[\"editor1\"].setData('-90 main battle tanks and BMP-2 Infantry combat vehicles (ICV) of the Indian Army during an exercise. The T-90 main battle tank, which is an evolution of the T-72 tank, is one of the most lethal tanks in existence. T-90 equips the Indian Army�s strike corps, which are tasked with taking the battle to the enemy with lightening strikes across the border and capturing and holding territory. Accompanying the T-90s will be the BMP-2 ICVs, which will carry soldiers into battle. ');");
				sleep 10;
				$sel->click_ok("id=buttonPost");
				sleep 10;

				print("----------- verify profile photo upload functionality ----------- ". "\n");
				$sel->click_ok("link=About Me");
				$sel->wait_for_page_to_load_ok("60000");
				$sel->title_is("Free Websites by India.com | Get Free Domain Name & Host Your Website on India.com");
				$sel->click_ok("id=pic_upload");
				$sel->type_ok("id=pic_upload", "C:\\Users\\sunil.nalawade.MAILDC\\Pictures\\Design (1).jpg");
				$sel->click_ok("id=buttonPost");
				$sel->wait_for_page_to_load_ok("60000");
				$sel->title_is("smith1 on India.com | Get Latest News & Blogs by Smith Airoli at smith1.beta.ipublish.india.com");
				my $img = $sel->get_attribute("css=figure.profileimg > img\@src");
				print($img . "\n");
				#$sel->open_ok($img);
				my $browser = LWP::UserAgent->new;
				my $response = $browser->get($img);
				print $response->code(), " (that means ", $response->message( ), " )\n";

				print("----------- Steps to check twitter sharing from Ipublish ----------- ". "\n");
				$sel->click_ok("//h3/a");
				$sel->wait_for_page_to_load_ok("60000");
				#$sel->title_is("tesxgjk fdfjkld | Latest Blogs by smith1 on India.com");
				my $Ipubtitle = $sel->get_text("css=h2");
				print($Ipubtitle . "\n");
				my $Ipuburl = $sel->get_location();
				print($Ipuburl . "\n");
				$sel->click_ok("//a[3]");

				$sel->wait_for_pop_up_ok("twitter_tweet", "30000");
				$sel->select_window_ok("name=twitter_tweet");
				WAIT: {
					for (1..60) {
						if (eval { $sel->is_element_present("//textarea") }) { pass; last WAIT }
						sleep(1);
					}
					fail("timeout");
				}
				my $txt = $sel->get_text("//textarea");
				$sel->click_ok("//input[\@value='Tweet']");
				$sel->wait_for_page_to_load_ok("60000");
				$sel->title_is("Share a link on Twitter");
				print($txt . "\n");
				my $txt1 = (split('#', $txt))[0];
				print($txt1 . "\n");
				my $title = (split('http://', $txt1))[0];
				print($title . "\n");
				my $url = (split('http://', $txt1))[1];
				$url = "http://".$url;
				print($url . "\n");
				sleep 10;
				$sel->select_window_ok("null");
				$sel->open_ok("$url");
				my $Bitlyurl = $sel->get_location();
				print($Bitlyurl . "\n");

				my $RESULT1 = $sel->get_eval($sel->get_eval("var result = '" . $Ipubtitle . "'=='" . $title . "';result;")&&("var result = '" . $Ipuburl . "'=='" . $Bitlyurl . "';result;"));
				print($RESULT1 . "\n");

		}



